import * as PublicHolidaysService from './public-holidays.service';
import axios from 'axios';
import * as config from '../config';

describe('getListOfPublicHolidays', () => {
    it('should retrieve a list of holidays', async () => {
        const axiosSpy = jest.spyOn(axios, 'get').mockResolvedValue({ data: [{ date: '2024-01-01', name: 'New Year' }] });
        const holidaylist = await PublicHolidaysService.getListOfPublicHolidays(2024, 'GB');
  
        expect(axiosSpy).toHaveBeenCalledWith(`${config.PUBLIC_HOLIDAYS_API_URL}/PublicHolidays/2024/GB`);
        expect(holidaylist).toEqual([{ date: '2024-01-01', name: 'New Year' }]);
    });

    it('returns an empty array if there is an error', async () => {
        jest.mock('../helpers', () => ({
            validateInput: jest.fn(() => {})
          }))
        const axiosSpy = jest.spyOn(axios, 'get').mockRejectedValue(new Error('Failed to fetch data'));
    
        const holidays = await PublicHolidaysService.getListOfPublicHolidays(2024, 'GB');
    
        expect(axiosSpy).toHaveBeenCalledWith(`${config.PUBLIC_HOLIDAYS_API_URL}/PublicHolidays/2024/GB`);
        expect(holidays).toEqual([]);
    });
})

describe('checkIfTodayIsPublicHoliday', () => {
    it('checks if today is a public holiday in a given country', async () => {
      const axiosSpy = jest.spyOn(axios, 'get').mockResolvedValue({ status: 200 });

      const isPublicHoliday = await PublicHolidaysService.checkIfTodayIsPublicHoliday('GB');

      expect(axiosSpy).toHaveBeenCalledWith(`${config.PUBLIC_HOLIDAYS_API_URL}/IsTodayPublicHoliday/GB`);
      expect(isPublicHoliday).toEqual(true);
    });

    it('returns false if there is an error', async () => {
      jest.spyOn(axios, 'get').mockRejectedValue(new Error('Failed to fetch data'));

      const isPublicHoliday = await PublicHolidaysService.checkIfTodayIsPublicHoliday('GB');

      expect(isPublicHoliday).toEqual(false);
    });
  });

  describe('getNextPublicHolidays', () => {
    it('fetches the next public holidays for a given country', async () => {
        jest.mock('../helpers', () => ({
            validateInput: jest.fn(() => {}),
            shortenPublicHoliday: jest.fn((holiday) => holiday)
          }))
        const axiosSpy = jest.spyOn(axios, 'get').mockResolvedValue({ data: [{ date: '2024-01-01', name: 'New Year' }] });
    
        const holidays = await PublicHolidaysService.getNextPublicHolidays('GB');
    
        expect(axiosSpy).toHaveBeenCalledWith(`${config.PUBLIC_HOLIDAYS_API_URL}/NextPublicHolidays/GB`);
        expect(holidays).toEqual([{ date: '2024-01-01', name: 'New Year' }]);
      });

    it('returns an empty array if there is an error', async () => {
      jest.spyOn(axios, 'get').mockRejectedValue(new Error('Failed to fetch data'));

      const holidays = await PublicHolidaysService.getNextPublicHolidays('GB');

      expect(holidays).toEqual([]);
    });
});


