import { getListOfPublicHolidays, getNextPublicHolidays, checkIfTodayIsPublicHoliday } from '../services/public-holidays.service';

describe('Integration Tests for Public Holidays Service', () => {
  it('should get a list of public holidays for a specific country and year', async () => {
    const year = 2024;
    const country = 'DE';

    const holidays = await getListOfPublicHolidays(year, country);

    expect(Array.isArray(holidays)).toBe(true);
    expect(holidays.length).toBeGreaterThan(0);
  });

  it('should get the next public holidays for a specific country', async () => {
    const country = 'DE';

    const holidays = await getNextPublicHolidays(country);
    
    expect(Array.isArray(holidays)).toBe(true);
    expect(holidays.length).toBeGreaterThan(0);
  });

  it('should check if today is a public holiday in a specific country', async () => {
    const country = 'DE';

    const isPublicHoliday = await checkIfTodayIsPublicHoliday(country);
    
    expect(typeof isPublicHoliday).toBe('boolean');
  });
});