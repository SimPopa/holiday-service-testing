import request from 'supertest';


describe('E2E Tests for Nager.Date API Endpoints', () => {
  it('should get country info for a specific country', async () => {
    const countryCode = 'US';

    const response = await request('https://date.nager.at')
      .get(`/api/v3/CountryInfo/${countryCode}`)
      .expect(200);

    expect(response.body).toHaveProperty('commonName');
    expect(response.body).toHaveProperty('officialName');
    expect(response.body).toHaveProperty('countryCode');
  });

  it('should get public holidays for a specific year and country', async () => {
    const year = 2024;
    const countryCode = 'US';

    const response = await request('https://date.nager.at')
      .get(`/api/v3/PublicHolidays/${year}/${countryCode}`)
      .expect(200);

    expect(Array.isArray(response.body)).toBe(true);
  });
});