import { SUPPORTED_COUNTRIES } from './config';
import { validateInput, shortenPublicHoliday } from './helpers'; // Import your functions
import { PublicHoliday, PublicHolidayShort } from './types';

 
describe('validateInput', () => {
  it('returns true for valid input', () => {
    expect(validateInput({ year: new Date().getFullYear(), country: 'GB' })).toBe(true);
  });

  it('throws an error for invalid country', () => {
    expect(() => validateInput({ country: 'XX' })).toThrowError('Country provided is not supported, received: XX');
  });

  it('throws an error for invalid year', () => {
    const currentYear = new Date().getFullYear();
    expect(() => validateInput({ year: currentYear - 1 })).toThrowError(`Year provided not the current, received: ${currentYear - 1}`);
  });
});

describe('shortenPublicHoliday', () => {
  it('shortens public holiday information correctly', () => {
    const holiday: PublicHoliday = {
      name: 'Christmas',
      localName: 'Christmas',
      date: '2024-12-25',
      countryCode: 'GB',
      fixed: true,
      global: true,
      counties: ['test','test1'],
      launchYear: 1970,
      types: ['test1', 'test2']
    };
    const expectedShortenedHoliday: PublicHolidayShort = {
      name: 'Christmas',
      localName: 'Christmas',
      date: '2024-12-25',
    };
    expect(shortenPublicHoliday(holiday)).toEqual(expectedShortenedHoliday);
  });
});