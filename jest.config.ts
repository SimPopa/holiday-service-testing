module.exports = {
    testEnvironment: 'node',
    preset: 'ts-jest',
    roots: ['./src'],
    coverageThreshold: {
      global: {
        branches: 85,
        functions: 85,
        lines: 85,
        statements: 85
      }
    },
    testPathIgnorePatterns: [
      "<rootDir>/test-e2e"
    ]
  };